'use strict';

// We are going to make the 'target image' draggable here
let targetIcon = document.querySelector('.draggable');
let currentDropPoint = null;

const handleDrag = (e) => {
  // prevents the default dragging behaviour of browser
  e.preventDefault();

  // place the element to the top of all other elements and make it absolute positioned
  document.body.append(e.target);
  e.target.style.position = 'absolute';

  // make it follow the cursor when cursor moves
  const drag = (e) => {
    targetIcon.style.left = e.clientX - xToSubtract + 'px';
    targetIcon.style.top = e.clientY - yToSubtract + 'px';

    // detect if there's a droppable under cursor
    /* The draggable target is at the top most. Nothing below will catch the mouse events, for which we'll need to hide the draggable target.
    To know if there's a draggable object underneath, we use a method that gives us the innermost element at given coordinate. */
    targetIcon.hidden = true;
    let dropPoint = null
    let elemUnder = document.elementFromPoint(e.clientX, e.clientY);

    if (elemUnder) dropPoint = elemUnder.closest('.drop-area');
    targetIcon.hidden = false;

    if (currentDropPoint) {
      currentDropPoint.classList.remove('highlight');
    }
    currentDropPoint = elemUnder;

    if (currentDropPoint) {
      currentDropPoint.classList.add('highlight');
    }
  };

  // for proper movement of draggable under cursor
  let xToSubtract = e.clientX - targetIcon.getBoundingClientRect().left;
  let yToSubtract = e.clientY - targetIcon.getBoundingClientRect().top;

  /* The browser checks whether the mouse is moving at a definite interval of time.
  This doesn't guarantee pixel perfect event detection. Setting the mousemove event on the image (which has a small dimension) might make the browser not register the event fast enough when the mouse is moving, and make the mouse move away from the image. Thus we set it on the document. */
  document.addEventListener('mousemove', drag);

  const removeAllHandlers = (e) => {
    document.removeEventListener('mousemove', drag);
  };

  e.target.addEventListener('mouseup', removeAllHandlers);
};

targetIcon.addEventListener('mousedown', handleDrag);
